<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CompanyModel;
use App\ContactModel;

class ContactsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contacts = ContactModel::JoinedContacts();
        return view('contacts', compact('contacts'));//, ['name' => 'James']);
    }
    
    //Filtered by a Company ID
    public function index_company($cid)
    {
        $c = CompanyModel::Lookup($cid)->get();
        $company = $c[0];//first index
        $contacts = ContactModel::ByCompany($cid)->get();
        return view('company_contacts', ['contacts' => $contacts, 'company' => $company]);//, ['name' => 'James']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('contact');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $contact = new ContactModel();
        $contact->company_id = $request->company_id;
        $contact->first_name = $request->first_name;
        $contact->last_name = $request->last_name;
        $contact->phone = $request->phone;
        $contact->email = $request->email;
    	$contact->save();
    	return redirect('/contacts');         
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $c = ContactModel::Lookup($id)->get();
        $contact = $c[0];//first index
        return view('edit-contact', compact('contact'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$c = ContactModel::Lookup($id)->get();
		$contact = $c[0];//first index
		$contact->company_id = $request->company_id;
		$contact->first_name = $request->first_name;
		$contact->last_name = $request->last_name;
		$contact->phone = $request->phone;
		$contact->email = $request->email;
		$contact->save();
		return redirect('/contacts'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		$c = ContactModel::Lookup($id)->get();
		$contact = $c[0];//first index
		$contact->delete();
		return redirect('/contacts');
    }
}
