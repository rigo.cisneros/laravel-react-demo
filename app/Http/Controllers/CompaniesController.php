<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CompanyModel;

class CompaniesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = CompanyModel::all();
        return view('companies', compact('companies'));//, ['name' => 'James']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('company');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $company = new CompanyModel();
        $company->name = $request->name;
        $company->address = $request->address;
        $company->city = $request->city;
        $company->state = $request->state;
        $company->zip = $request->zip;
        $company->phone = $request->phone;
    	$company->save();
    	return redirect('/companies');         
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $c = CompanyModel::Lookup($id)->get();
        $company = $c[0];//first index
        //$company = CompanyModel::Lookup($id)->get();
        return view('edit-company', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$c = CompanyModel::Lookup($id)->get();
		$company = $c[0];//first index
		$company->name = $request->name;
		$company->address = $request->address;
		$company->city = $request->city;
		$company->state = $request->state;
		$company->zip = $request->zip;
		$company->phone = $request->phone;
		$company->save();
		return redirect('/companies'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		$c = CompanyModel::Lookup($id)->get();
		$company = $c[0];//first index
		$company->delete();
		return redirect('/companies');
    }
}
