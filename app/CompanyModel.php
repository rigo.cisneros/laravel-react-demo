<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyModel extends Model
{
	//protected $connection = 'sqlite';//specific, will use default otherwise
	protected $table = 'companies';

	//Scope by Lookup
	public function scopeLookup($query, $id)
    {
        return $query->where('id', '=', $id);
    }
	
}
