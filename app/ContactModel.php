<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ContactModel extends Model
{
	//protected $connection = 'sqlite';//specific, will use default otherwise
	protected $table = 'contacts';
	
	//Scope by Lookup
	public function scopeLookup($query, $id)
    {
        return $query->where('id', '=', $id);
    }
	
	//Scope by company
	public function scopeByCompany($query, $id)
    {
        return $query->where('company_id', '=', $id);
    }
	
	//Scope with join
	public function scopeJoinedContacts($query)
	{
        return DB::table('contacts')
            ->join('companies', 'contacts.company_id', '=', 'companies.id')
            ->select('contacts.*', 'companies.name')
            ->get();
	}
	
}
