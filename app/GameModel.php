<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GameModel extends Model
{
	protected $table = 'games';

	//Scope by Lookup
	public function scopeLookup($query, $id)
    {
        return $query->where('id', '=', $id);
    }
    
}
