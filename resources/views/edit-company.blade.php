@extends('master');
@section('content')
    <div class="title m-b-md">
        Edit Company
    </div>

    <div class="links">
        <a href="../../">Home</a>
        <a href="../../companies">Companies</a>
        <a href="../../contacts">Contacts</a>
        <a href="../../company">Create Company</a>
        <a href="../../contact">Create Contact</a>
    </div>

    <!-- Entry START-->
    <br><br>
    <form method="POST" action="./{{ $company->id }}">
        <div class="form-group flex-center">

        <table>
            <tr><td>ID: </td><td><input name="name" value="{{ $company->id }}" class="form-control" readonly></input></td></tr>
            <tr><td>Name: </td><td><input name="name" value="{{ $company->name }}" class="form-control"></input></td></tr>
            <tr><td>Address: </td><td><input name="address" value="{{ $company->address }}" class="form-control"></input></td></tr>
            <tr><td>City: </td><td><input name="city" value="{{ $company->city }}" class="form-control"></input></td></tr>
            <tr><td>State: </td><td><input name="state" value="{{ $company->state }}" class="form-control"></input></td></tr>
            <tr><td>Zip: </td><td><input name="zip" value="{{ $company->zip }}" class="form-control"></input></td></tr>
            <tr><td>Phone: </td><td><input name="phone" value="{{ $company->phone }}" class="form-control"></input></td></tr>
        </table>
        </div>
    
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Update Company</button>
        </div>
    {{ csrf_field() }}
    </form>                
    <!-- Entry END -->
@endsection