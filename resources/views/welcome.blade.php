@extends('master');
@section('content')
    <div class="title m-b-md">
        Laravel-React-Demo
    </div>
    
    <div class="links">
        <a href="./">Home</a>
        <a href="./companies">Companies</a>
        <a href="./contacts">Contacts</a>
        <a href="./company">Create Company</a>
        <a href="./contact">Create Contact</a>
        <!--<a href="https://laravel.com/docs">Documentation</a>
        <a href="https://laracasts.com">Laracasts</a>
        <a href="https://laravel-news.com">News</a>
        <a href="https://nova.laravel.com">Nova</a>
        <a href="https://forge.laravel.com">Forge</a>
        <a href="https://github.com/laravel/laravel">GitHub</a>-->
    </div>
        
    <!-- Display some Services for example -->
    <h1>Services we offer</h1>
    @if(count($services) > 0)
        <ul>
        @foreach($services as $service)
            <li>{{$service}}</li>
        @endforeach
        </ul>
    @endif
        
@endsection